<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\IO;
use RBS\Selifa\IComponent;
use RBS\Selifa\IConfigurationCache;
use RBS\Selifa\ISingletonGetter;
use RBS\Selifa\Traits\OptionableSingleton;
use RBS\Selifa\IO\Driver\ICacheDriver;
use SelifaException;

define('CACHE_INITIALIZATION_ERROR',SELIFA_EXCEPTION_SYSTEM_START_CODE + 60);
define('CACHE_DRIVER_NOT_EXISTS',SELIFA_EXCEPTION_SYSTEM_START_CODE + 61);
define('CACHE_DRIVER_INITIALIZATION_ERROR',SELIFA_EXCEPTION_SYSTEM_START_CODE + 62);
define('CACHE_DRIVER_DISCONNECTED_ERROR',SELIFA_EXCEPTION_SYSTEM_START_CODE + 63);

/**
 * Class Cache
 *
 * @package RBS\Selifa\IO
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class Cache implements IComponent, IConfigurationCache, ISingletonGetter
{
    use OptionableSingleton;

    /**
     * @var null|ICacheDriver
     */
    private $_Driver = null;

    /**
     * @var null|ICacheEncryptor
     */
    private $_Encryptor = null;

    /**
     * @var string
     */
    private $_DriverPath = '';

    /**
     * @var bool
     */
    private $_UseSignature = false;

    /**
     * @var string
     */
    private $_HashKey = '';

    /**
     * @var bool
     */
    private $_Secure = false;

    /**
     * @var string
     */
    private $_EncryptionKey = '';

    /**
     * @param array $options
     * @throws SelifaException
     */
    protected function _OnInit($options)
    {
        $driverNs = "RBS\\Selifa\\IO\\Driver\\";

        $driverName = 'apc';
        if (isset($options['driver']))
            $driverName = trim($options['driver']);
        else if (isset($options['Driver']))
            $driverName = trim($options['Driver']);
        if (strpos($driverName,"\\",0) > -1)
            $className = $driverName;
        else
            $className = ($driverNs.strtolower($driverName).'CacheDriver');

        $drvObj = new $className();
        if (!($drvObj instanceof ICacheDriver))
            throw new SelifaException(CACHE_INITIALIZATION_ERROR,$className.' does not implements ICacheDriver.');

        if (isset($options['UseSignature']))
            $this->_UseSignature = (bool)$options['UseSignature'];
        if (isset($options['HashKey']))
            $this->_HashKey = trim($options['HashKey']);
        if ($this->_UseSignature)
        {
            if ($this->_HashKey == '')
                throw new SelifaException(CACHE_INITIALIZATION_ERROR,'Please specify hash key to use signature.');
        }

        if (isset($options['Secure']))
            $this->_Secure = (bool)$options['Secure'];
        if (isset($options['EncryptionKey']))
            $this->_EncryptionKey = trim($options['EncryptionKey']);
        if ($this->_Secure)
        {
            if (isset($options['EncryptionClass']))
            {
                $eClassName = trim($options['EncryptionClass']);
                if (!class_exists($eClassName,true))
                    throw new SelifaException(CACHE_INITIALIZATION_ERROR,'Specified encryption class does not exists.');
                $eObj = new $eClassName();
                if (!($eObj instanceof ICacheEncryptor))
                    throw new SelifaException(CACHE_INITIALIZATION_ERROR,'Specified encryption class does not implement ICacheEncryptor.');
                $this->_Encryptor = $eObj;
            }
        }

        $opts = [];
        if (isset($options['options']))
            $opts = $options['options'];
        else if (isset($options['Options']))
            $opts = $options['Options'];
        $drvObj->Initialize($opts);
        $this->_Driver = $drvObj;
    }

    /**
     * @return null|ICacheDriver
     */
    public function GetDriverObject()
    {
        return $this->_Driver;
    }

    /**
     * @param ICacheEncryptor $encryptor
     * @return Cache
     */
    public function SetEncryptor(ICacheEncryptor $encryptor)
    {
        $this->_Encryptor = $encryptor;
        return $this;
    }

    /**
     * Store data in cache specified by $key for a limited time specified by $expiryTime.
     *
     * @param string $key cache key.
     * @param mixed $value data to be stored.
     * @param int $expiryTime Time to live for data in seconds.
     */
    public function Write($key, $value, $expiryTime=60)
    {
        $this->_Driver->Save($key,$value,$expiryTime);
    }

    /**
     * Check whether specific data is exists or not.
     *
     * @param string $key cache key.
     * @return boolean
     */
    public function Available($key)
    {
        return $this->_Driver->Exists($key);
    }

    /**
     * Load data from cache specified by $key. Returns $default if data doest not exists.
     *
     * @param string $key cache key.
     * @param mixed|null $default Default value if cache's value is empty.
     * @return mixed|null
     */
    public function Read($key, $default = null)
    {
        $data = $this->_Driver->Load($key);
        if ($data === null)
            return $default;
        else
            return $data;
    }

    /**
     * Remove data in cache specified by $Key
     *
     * @param string $key cache key
     * @return boolean
     */
    public function Delete($key)
    {
        $this->_Driver->Remove($key);
    }

    /**
     * Clear all stored data.
     */
    public function Flush()
    {
        $this->_Driver->Clear();
    }
}
?>