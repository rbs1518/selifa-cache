<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\IO\Driver;
use SelifaException;

/**
 * Class fsCacheDriver
 *
 * @package RBS\Selifa\IO\Driver
 * @copyright 2019-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class fsCacheDriver implements ICacheDriver
{
    /**
     * @var string
     */
    private $_Path = 'temps/cache/';

    /**
     * @var string
     */
    private $_Prefix = 'cache.';

    /**
     * @var bool
     */
    private $_EncodeToJSON = false;

    /**
     * @param array $options
     * @throws SelifaException
     */
    public function Initialize($options)
    {
        if (isset($options['Path']))
            $this->_Path = trim($options['Path']);

        $this->_Path = (SELIFA_ROOT_PATH.'/'.$this->_Path);
        if (substr($this->_Path,-1,1) != '/')
            $this->_Path .= '/';

        if (!file_exists($this->_Path))
            throw new SelifaException(CACHE_DRIVER_INITIALIZATION_ERROR,'Cache path does not exists.');

        if (!is_writable($this->_Path))
            throw new SelifaException(CACHE_DRIVER_INITIALIZATION_ERROR,'Cache path is not writeable.');

        if (isset($options['Prefix']))
            $this->_Prefix = trim($options['Prefix']);

        if (isset($options['EncodeToJSON']))
            $this->_EncodeToJSON = (bool)$options['EncodeToJSON'];
    }

    /**
     * Store data in cache specified by $key for a limited time specified by $expiryTime.
     *
     * @param string $key cache key.
     * @param mixed $value data to be stored.
     * @param int $expiryTime Time to live for data in seconds.
     * @return mixed
     */
    public function Save($key, $value, $expiryTime)
    {
        $fn = ($this->_Path.$this->_Prefix.$key);
        if (!is_object($value) && !is_array($value))
            file_put_contents($fn,$value);
        else
        {
            if ($this->_EncodeToJSON)
                $aVal = json_encode($value);
            else
                $aVal = serialize($value);
            file_put_contents($fn,$aVal);
        }
        return true;
    }

    /**
     * Load data from cache specified by $key
     *
     * @param string $key cache key.
     * @return mixed|null
     */
    public function Load($key)
    {
        $fn = ($this->_Path.$this->_Prefix.$key);
        if (file_exists($fn))
            return file_get_contents($fn);
        else
            return null;
    }

    /**
     * Check whether specific data is exists or not.
     *
     * @param string $key cache key.
     * @return boolean
     */
    public function Exists($key)
    {
        $fn = ($this->_Path.$this->_Prefix.$key);
        return file_exists($fn);
    }

    /**
     * Remove data in cache specified by $Key
     *
     * @param string $key cache key.
     * @return boolean
     */
    public function Remove($key)
    {
        $fn = ($this->_Path.$this->_Prefix.$key);
        if (file_exists($fn))
            unlink($fn);
        return true;
    }

    /**
     * Clear all stored data.
     *
     * @return boolean
     */
    public function Clear()
    {
        if (!is_dir($this->_Path))
            return false;

        $dir_handle = opendir($this->_Path);
        if (!$dir_handle)
            return false;

        while($file = readdir($dir_handle))
        {
            if ($file != "." && $file != "..")
            {
                if (!is_dir($this->_Path.$file))
                    unlink($this->_Path.$file);
            }
        }
        closedir($dir_handle);
        return true;
    }
}
?>