<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\IO\Driver;
use SelifaException;

/**
 * Class apcCacheDriver
 *
 * @package RBS\Selifa\IO\Driver
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class apcCacheDriver implements ICacheDriver
{
    /**
     * @var bool
     */
    private $_UseAPCU = false;

    /**
     * @param array $options
     * @throws SelifaException
     */
    public function Initialize($options)
    {
        if (function_exists('apcu_store'))
            $this->_UseAPCU = true;
        else if (!function_exists('apc_store'))
            throw new SelifaException(CACHE_DRIVER_INITIALIZATION_ERROR,'APC/APCU extension is not installed.');

        $disableWarning = false;
        if (isset($options['DisableWarning']))
            $disableWarning = (bool)$options['DisableWarning'];

        if (!$disableWarning)
        {
            $isCLI = (php_sapi_name() === 'cli');
            if ($isCLI)
                echo "Cache Warning: APC/APCU module by default is disabled. Enable it by putting 'apc.enable_cli = 1' in PHP configuration file.\n";
        }
    }

    /**
     * Store data in cache specified by $key for a limited time specified by $expiryTime.
     *
     * @param string $key cache key.
     * @param mixed $value data to be stored.
     * @param int $expiryTime Time to live for data in seconds.
     * @return boolean
     */
    public function Save($key, $value, $expiryTime)
    {
        if ($this->_UseAPCU)
            return apcu_store($key,$value,$expiryTime);
        else
            return apc_store($key,$value,$expiryTime);
    }

    /**
     * Load data from cache specified by $key
     *
     * @param string $key cache key.
     * @return mixed|null
     */
    public function Load($key)
    {
        $success = false;
        if ($this->_UseAPCU)
            $temp  = apcu_fetch($key,$success);
        else
            $temp  = apc_fetch($key,$success);
        if (!$success)
            return null;
        else
            return $temp;
    }

    /**
     * Check whether specific data is exists or not.
     *
     * @param string $key cache key.
     * @return boolean
     */
    public function Exists($key)
    {
        if ($this->_UseAPCU)
            return apcu_exists($key);
        else
            return apc_exists($key);
    }

    /**
     * Remove data in cache specified by $Key
     *
     * @param $key \RBS\Selifa\IO\cache key.
     * @return boolean
     */
    public function Remove($key)
    {
        if ($this->_UseAPCU)
            return apcu_delete($key);
        else
            return apc_delete($key);
    }

    /**
     * Clear all stored data.
     *
     * @return boolean
     */
    public function Clear()
    {
        if ($this->_UseAPCU)
            return apcu_clear_cache();
        else
            return apc_clear_cache();
    }
}
?>