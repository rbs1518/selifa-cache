<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\IO\Driver;

/**
 * Interface ICacheDriver
 *
 * @package RBS\Selifa\IO\Driver
 * @copyright 2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
interface ICacheDriver
{
    /**
     * @param array $options
     */
    public function Initialize($options);

    /**
     * Store data in cache specified by $key for a limited time specified by $expiryTime.
     *
     * @param string $key cache key.
     * @param mixed $value data to be stored.
     * @param int $expiryTime Time to live for data in seconds.
     * @return mixed
     */
    public function Save($key,$value,$expiryTime);

    /**
     * Load data from cache specified by $key
     *
     * @param string $key cache key.
     * @return mixed|null
     */
    public function Load($key);

    /**
     * Check whether specific data is exists or not.
     *
     * @param string $key cache key.
     * @return boolean
     */
    public function Exists($key);

    /**
     * Remove data in cache specified by $Key
     *
     * @param string $key cache key.
     * @return boolean
     */
    public function Remove($key);

    /**
     * Clear all stored data.
     *
     * @return boolean
     */
    public function Clear();
}
?>