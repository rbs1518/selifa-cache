<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\IO\Driver;
use SelifaException;
use Redis;

/**
 * Class redisCacheDriver
 *
 * @package RBS\Selifa\IO\Driver
 * @copyright 2019-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class redisCacheDriver implements ICacheDriver
{
    /**
     * @var string
     */
    private $_Host = '127.0.0.1';

    /**
     * @var int
     */
    private $_Port = 6379;

    /**
     * @var int
     */
    private $_DBIndex = 0;

    /**
     * @var string
     */
    private $_Password = '';

    /**
     * @var string
     */
    private $_Prefix = 'selifa:';

    /**
     * @var bool
     */
    private $_Precheck = false;

    /**
     * @var null|Redis
     */
    private $_Redis = null;

    /**
     * @param array $options
     * @throws SelifaException
     */
    public function Initialize($options)
    {
        if (!extension_loaded('redis'))
            throw new SelifaException(CACHE_DRIVER_INITIALIZATION_ERROR,'Redis extension is not installed.');

        if (isset($options['Host']))
            $this->_Host = trim($options['Host']);
        if (isset($options['Port']))
            $this->_Port = (int)$options['Port'];
        if (isset($options['DB']))
            $this->_DBIndex = (int)$options['DB'];
        if (isset($options['Password']))
            $this->_Password = trim($options['Password']);
        if (isset($options['Prefix']))
            $this->_Prefix = trim($options['Prefix']);
        if (isset($options['Precheck']))
            $this->_Precheck = (bool)$options['Precheck'];

        $this->_Redis = new Redis();

        if ($this->_Port == 0)
            $this->_Redis->connect($this->_Host);
        else
            $this->_Redis->connect($this->_Host,$this->_Port);

        if ($this->_Password != '')
            $this->_Redis->auth($this->_Password);

        $this->_Redis->select($this->_DBIndex);

        if ($this->_Prefix != '')
            $this->_Redis->setOption(Redis::OPT_PREFIX,$this->_Prefix);

        $this->_Redis->setOption(Redis::OPT_SERIALIZER,Redis::SERIALIZER_NONE);
    }

    /**
     *
     */
    function __destruct()
    {
        if ($this->_Redis !== null)
            $this->_Redis->close();
    }

    /**
     * @return null|Redis
     */
    public function GetRedisObject()
    {
        return $this->_Redis;
    }

    /**
     * Store data in cache specified by $key for a limited time specified by $expiryTime.
     *
     * @param string $key cache key.
     * @param mixed $value data to be stored.
     * @param int $expiryTime Time to live for data in seconds.
     * @throws SelifaException
     * @return mixed
     */
    public function Save($key, $value, $expiryTime)
    {
        if ($this->_Precheck)
        {
            $resp = $this->_Redis->ping();
            if (($resp === '+PONG') || ($resp === true))
                $this->_Redis->set($key,$value,$expiryTime);
            else
                throw new SelifaException(CACHE_DRIVER_DISCONNECTED_ERROR,'Redis server disconnected.');
        }
        else
            $this->_Redis->set($key,$value,$expiryTime);
    }

    /**
     * Load data from cache specified by $key
     *
     * @param string $key cache key.
     * @throws SelifaException
     * @return mixed|null
     */
    public function Load($key)
    {
        if ($this->_Precheck)
        {
            $resp = $this->_Redis->ping();
            if (($resp === '+PONG') || ($resp === true))
                return $this->_Redis->get($key);
            else
                throw new SelifaException(CACHE_DRIVER_DISCONNECTED_ERROR,'Redis server disconnected.');
        }
        else
            return $this->_Redis->get($key);
    }

    /**
     * Check whether specific data is exists or not.
     *
     * @param string $key cache key.
     * @throws SelifaException
     * @return boolean
     */
    public function Exists($key)
    {
        if ($this->_Precheck)
        {
            $resp = $this->_Redis->ping();
            if (($resp === '+PONG') || ($resp === true))
                return $this->_Redis->exists($key);
            else
                throw new SelifaException(CACHE_DRIVER_DISCONNECTED_ERROR,'Redis server disconnected.');
        }
        else
            return $this->_Redis->exists($key);
    }

    /**
     * Remove data in cache specified by $Key
     *
     * @param string $key cache key.
     * @throws SelifaException
     * @return boolean
     */
    public function Remove($key)
    {
        if ($this->_Precheck)
        {
            $resp = $this->_Redis->ping();
            if (($resp === '+PONG') || ($resp === true))
                $this->_Redis->del($key);
            else
                throw new SelifaException(CACHE_DRIVER_DISCONNECTED_ERROR,'Redis server disconnected.');
        }
        else
            $this->_Redis->del($key);
    }

    /**
     * Clear all stored data.
     *
     * @throws SelifaException
     * @return boolean
     */
    public function Clear()
    {
        if ($this->_Precheck)
        {
            $resp = $this->_Redis->ping();
            if (($resp === '+PONG') || ($resp === true))
                return $this->_Redis->flushDB();
            else
                throw new SelifaException(CACHE_DRIVER_DISCONNECTED_ERROR,'Redis server disconnected.');
        }
        else
            return $this->_Redis->flushDB();
    }
}
?>